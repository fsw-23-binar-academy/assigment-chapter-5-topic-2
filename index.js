const fs = require("fs");
const DB = "./db/products.json";

const getProducts = () => {
  return new Promise((resolve, reject) => {
    fs.readFile(DB, "utf-8", (err, data) => {
      if (err) {
        reject("Error datanya");
      } else {
        resolve(JSON.parse(data));
      }
    });
  });
};

const createNewData = (products, params) => {
  const newId = products[products.length - 1].id + 1;
  const newProduct = { id: newId, ...params };
  products.push(newProduct);

  return new Promise((resolve, reject) => {
    fs.writeFile(DB, JSON.stringify(products), (err, data) => {
      if (err) {
        reject("Error datanya");
      } else {
        resolve(products);
      }
    });
  });
};

const replaceData = (newData) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(DB, JSON.stringify(newData), (err, data) => {
      if (err) {
        reject("Error datanya");
      } else {
        resolve(newData);
      }
    });
  });
};

if (process.argv[2] == "getProducts") {
  const products = getProducts();
  products
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.log("Terjadi error", err);
    });
}

if (process.argv[2] == "getDetailProduct") {
  //how to run : node index.js getDetailProduct 2
  const idProduct = process.argv[3];
  const dataProducts = getProducts();
  dataProducts
    .then((data) => {
      const product = data.find((product) => product.id == idProduct);
      return product;
    })
    .then((data) => {
      console.log("detail datanya adalah", data);
    })
    .catch((err) => {
      console.log("Terjadi error", err);
    });
}

if (process.argv[2] == "addProduct") {
  //how to run : node index.js addProduct '{"name":"Cable USB Type C to C ", "price":1000000}'
  const params = JSON.parse(process.argv[3]);
  console.log("params", params);
  const dataProducts = getProducts();
  dataProducts
    .then((data) => {
      return createNewData(data, params);
    })
    .then((data) => {
      console.log("data baru", data);
    })
    .catch((err) => {
      console.log("Terjadi error", err);
    });
}

if (process.argv[2] == "editProduct") {
  //how to run : node index.js editProduct 2 '{"price":2000000}'
  const idProduct = process.argv[3];
  const params = JSON.parse(process.argv[4]);
  console.log("params", params);
  const dataProducts = getProducts();
  dataProducts
    .then((data) => {
      data.forEach((product, key) => {
        if (product.id == idProduct) {
          data[key] = { ...product, ...params };
        }
      });
      return data;
    })
    .then((data) => {
      return replaceData(data);
    })
    .then((data) => {
      console.log("data baru", data);
    })
    .catch((err) => {
      console.log("Terjadi error", err);
    });
}

if (process.argv[2] == "deleteProduct") {
  //how to run : node index.js deleteProduct 2
  const idProduct = process.argv[3];
  const dataProducts = getProducts();
  dataProducts
    .then((data) => {
      const newData = data.filter((product) => product.id != idProduct);
      return newData;
    })
    .then((data) => {
      return replaceData(data);
    })
    .then((data) => {
      console.log("data baru", data);
    })
    .catch((err) => {
      console.log("Terjadi error", err);
    });
}
